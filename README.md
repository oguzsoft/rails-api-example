# Rails Api Application

## Creating rails-api-example application

```bash
rails new rails-api-example --api -B -T
cd rails-api-example
```
## Open Gemfile and add this gems

```ruby
group :development do
    gem 'hirb',
    gem 'faker'
end
```
    
```bash
bundle install
```
    
```bash
rails g scaffold Book title author
rails db:migrate
```

### In app/models/book.rb
```ruby
class Book < ActiveRecord
    validates :title, presence: true
    validates :author, presence: true
end
```

### Seeding database using faker gem 'db/seed.rb'
```ruby
10.times do
Book.create({
    title: Faker::Book.title,
    author: Faker::Book.author
})
end
```
    
```bash
rails db:seed
```

### You can see all books using hirb gem in rails console. Open Terminal

```bash
rails c
Hirb.enable
Book.all
```

![output.png](./output.png)

### Run Rails server 
```bash
rails s
```

#### Check ```http://localhost:3000/books```  Safari, Chrome, Mozilla or other apps (Postman, httpie, etc...). You will see all books. 

## CORS error solution
### in Gemfile 
```ruby
gem 'rack-cors'
```
```bash
bundle install
```

### in config/initializers/cors.rb
```ruby
Rails.application.config.middleware.insert_before 0, Rack::Cors do
  allow do
    origins 'localhost:4200'
    resource '*',
      headers: :any,
      methods: %i(get post put patch delete options head)
  end
end
```
### restart rails server




    
    
    
    


